﻿using bomWeatherParser;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace bomWeatherParser
{
    public static class BomWeatherParser
    {        
        public static string bomCsvToJson(string csvPath, char separator)
        {
            IDictionary<DateTime, BomDayData> dailyWeather = new Dictionary<DateTime, BomDayData>();

            using (var reader = new StreamReader(csvPath))
            {                
                reader.ReadLine(); // Ignore csv headers
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    string[] columns = line.Split(separator);

                    int year, month, day;
                    int.TryParse(columns[2], out year);
                    int.TryParse(columns[3], out month);
                    int.TryParse(columns[4], out day);

                    decimal rainfall;
                    if (Decimal.TryParse(columns[5], out rainfall))
                    {
                        BomDayData dayData = new BomDayData(rainfall);
                        DateTime dateIndex = new DateTime(year, month, day);
                        dailyWeather.Add(new KeyValuePair<DateTime, BomDayData>(dateIndex, dayData));
                    }
                }
            }

            WeatherBlob weather = new WeatherBlob(dailyWeather);

            return weather.getJson();
        }
    }
}
