﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bomWeatherParser
{

    class BomJsonConverter : JsonConverter
    {
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            WeatherBlob weatherData = value as WeatherBlob;
            writer.WriteStartObject();
            writer.WritePropertyName("WeatherData");
            writer.WriteStartObject();
            foreach (var data in weatherData.yearlyWeatherData)
            {
                WeatherDataForYear year = data.Value;
                writer.WritePropertyName("WeatherDataForYear");
                writer.WriteStartObject();
                writer.WritePropertyName("Year");
                writer.WriteValue(year.year.ToString());
                WritePeriod(writer, year);

                writer.WritePropertyName("MonthlyAggregates");
                writer.WriteStartObject();
                foreach (var month in year.months)
                {
                    if (month != null)
                    {
                        writer.WritePropertyName("WeatherDataForMonth");
                        writer.WriteStartObject();
                        writer.WritePropertyName("Month");
                        writer.WriteValue(CultureInfo.InvariantCulture.DateTimeFormat.GetMonthName(month.month));
                        WritePeriod(writer, month);
                        writer.WriteEndObject();
                    }
                }
                writer.WriteEndObject();
                writer.WriteEndObject();
            }
            writer.WriteEndObject();
            writer.WriteEndObject();
        }

        private void WritePeriod(JsonWriter writer, WeatherDataForPeriod period)
        {
            writer.WritePropertyName("FirstRecordedDate");
            writer.WriteValue(String.Format("{0:yyyy-MM-dd}", period.firstRecordedDate));
            writer.WritePropertyName("LastRecordedDate");
            writer.WriteValue(String.Format("{0:yyyy-MM-dd}", period.lastRecordedDate));
            writer.WritePropertyName("TotalRainfall");
            writer.WriteValue(period.totalRainfall.ToString());
            writer.WritePropertyName("AverageDailyRainfall");
            writer.WriteValue(string.Format("{0:N9}", period.averageDailyRainfall));
            writer.WritePropertyName("DaysWithNoRainfall");
            writer.WriteValue(period.daysWithNoRainfall.ToString());
            writer.WritePropertyName("DaysWithRainfall");
            writer.WriteValue(period.daysWithRainfall.ToString());
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            // TODO if we want to deserialize the json in future
            return null;
        }

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(WeatherBlob);
        }
    }
}
