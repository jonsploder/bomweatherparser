﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using bomWeatherParser;
using System.IO;
using System.Text.RegularExpressions;

namespace bomUnitTests
{
    [TestClass]
    public class UnitTests
    {
        // FUTURE CONSIDERATIONS:
        // Do we care about duplicate dates in the data
        // Empty rows/newlines in the csv

        // Do we really care if the data isn't sorted to sort the result? (It is in the BOM output)
        //[TestMethod]
        //public void csvUnordered()
        //{
        //    // For example year 1900, 1901, 1899
        //    char separator = ',';
        //    string csvPath = "../../../exampleProvidedData.csv";
        //    string json = BomWeatherParser.bomCsvToJson(csvPath, separator);
        //    csvPath = "../../../exampleData_Shuffled.csv";
        //    string jsonShuffled = BomWeatherParser.bomCsvToJson(csvPath, separator);
        //    Assert.AreEqual(json, jsonShuffled);
        //}

        [TestMethod]
        public void midYear()
        {
            // Want to check for no extra empty fields in JSON if we start before january and end before december
            char separator = ',';
            string csvPath = "../../../MidYear.csv";
            string json = BomWeatherParser.bomCsvToJson(csvPath, separator);
            string expected =
@"{
  ""WeatherData"": {
    ""WeatherDataForYear"": {
      ""Year"": ""2018"",
      ""FirstRecordedDate"": ""2018-05-25"",
      ""LastRecordedDate"": ""2018-06-05"",
      ""TotalRainfall"": ""12.2"",
      ""AverageDailyRainfall"": ""1.016666667"",
      ""DaysWithNoRainfall"": ""6"",
      ""DaysWithRainfall"": ""6"",
      ""MonthlyAggregates"": {
        ""WeatherDataForMonth"": {
          ""Month"": ""May"",
          ""FirstRecordedDate"": ""2018-05-25"",
          ""LastRecordedDate"": ""2018-05-31"",
          ""TotalRainfall"": ""5.0"",
          ""AverageDailyRainfall"": ""0.714285714"",
          ""DaysWithNoRainfall"": ""5"",
          ""DaysWithRainfall"": ""2""
        },
        ""WeatherDataForMonth"": {
          ""Month"": ""June"",
          ""FirstRecordedDate"": ""2018-06-01"",
          ""LastRecordedDate"": ""2018-06-05"",
          ""TotalRainfall"": ""7.2"",
          ""AverageDailyRainfall"": ""1.440000000"",
          ""DaysWithNoRainfall"": ""1"",
          ""DaysWithRainfall"": ""4""
        }
      }
    }
  }
}";
            string sanitizedExpected = Regex.Replace(expected, @"\s+", String.Empty);
            string sanitizedActual = Regex.Replace(json, @"\s+", String.Empty);
            Assert.AreEqual(sanitizedExpected, sanitizedActual);
        }

        [TestMethod]
        public void yearlyTickover()
        {
            // Also tests FirstRecordedDate == LastRecordedDate for 2018.
            char separator = ',';
            string csvPath = "../../../YearlyTickover.csv";
            string json = BomWeatherParser.bomCsvToJson(csvPath, separator);
            string expected =
@"{
  ""WeatherData"": {
    ""WeatherDataForYear"": {
      ""Year"": ""2017"",
      ""FirstRecordedDate"": ""2017-12-30"",
      ""LastRecordedDate"": ""2017-12-31"",
      ""TotalRainfall"": ""9.8"",
      ""AverageDailyRainfall"": ""4.900000000"",
      ""DaysWithNoRainfall"": ""0"",
      ""DaysWithRainfall"": ""2"",
      ""MonthlyAggregates"": {
        ""WeatherDataForMonth"": {
          ""Month"": ""December"",
          ""FirstRecordedDate"": ""2017-12-30"",
          ""LastRecordedDate"": ""2017-12-31"",
          ""TotalRainfall"": ""9.8"",
          ""AverageDailyRainfall"": ""4.900000000"",
          ""DaysWithNoRainfall"": ""0"",
          ""DaysWithRainfall"": ""2""
        }
      }
    },
    ""WeatherDataForYear"": {
      ""Year"": ""2018"",
      ""FirstRecordedDate"": ""2018-01-01"",
      ""LastRecordedDate"": ""2018-01-01"",
      ""TotalRainfall"": ""0.6"",
      ""AverageDailyRainfall"": ""0.600000000"",
      ""DaysWithNoRainfall"": ""0"",
      ""DaysWithRainfall"": ""1"",
      ""MonthlyAggregates"": {
        ""WeatherDataForMonth"": {
          ""Month"": ""January"",
          ""FirstRecordedDate"": ""2018-01-01"",
          ""LastRecordedDate"": ""2018-01-01"",
          ""TotalRainfall"": ""0.6"",
          ""AverageDailyRainfall"": ""0.600000000"",
          ""DaysWithNoRainfall"": ""0"",
          ""DaysWithRainfall"": ""1""
        }
      }
    }
  }
}";
            string sanitizedExpected = Regex.Replace(expected, @"\s+", String.Empty);
            string sanitizedActual = Regex.Replace(json, @"\s+", String.Empty);
            Assert.AreEqual(sanitizedExpected, sanitizedActual);
        }

        [TestMethod]
        public void discountEmptyData()
        {
            // June 24th 2019 has no data point in the csv
            char separator = ',';
            string csvPath = "../../../2018Data.csv";
            string json = BomWeatherParser.bomCsvToJson(csvPath, separator);
            Assert.IsTrue(json.Contains("\"DaysWithNoRainfall\": \"10\"") &&
                          json.Contains("\"DaysWithRainfall\": \"19\""));
        }

        [TestMethod]
        public void january2019()
        {
            char separator = ',';
            string csvPath = "../../../2019JanData.csv";

            string json = BomWeatherParser.bomCsvToJson(csvPath, separator);
            string expected =
@"{
  ""WeatherData"": {
    ""WeatherDataForYear"": {
      ""Year"": ""2019"",
      ""FirstRecordedDate"": ""2019-01-01"",
      ""LastRecordedDate"": ""2019-01-31"",
      ""TotalRainfall"": ""48.8"",
      ""AverageDailyRainfall"": ""1.574193548"",
      ""DaysWithNoRainfall"": ""21"",
      ""DaysWithRainfall"": ""10"",
      ""MonthlyAggregates"": {
        ""WeatherDataForMonth"": {
          ""Month"": ""January"",
          ""FirstRecordedDate"": ""2019-01-01"",
          ""LastRecordedDate"": ""2019-01-31"",
          ""TotalRainfall"": ""48.8"",
          ""AverageDailyRainfall"": ""1.574193548"",
          ""DaysWithNoRainfall"": ""21"",
          ""DaysWithRainfall"": ""10""
        }
      }
    }
  }
}";

            string sanitizedExpected = Regex.Replace(expected, @"\s+", String.Empty);
            string sanitizedActual = Regex.Replace(json, @"\s+", String.Empty);
            Assert.AreEqual(sanitizedExpected, sanitizedActual);
        }
    }
}
