﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace bomWeatherParser
{
    public class WeatherBlob
    {
        public IDictionary<int, WeatherDataForYear> yearlyWeatherData = new Dictionary<int, WeatherDataForYear>();

        public WeatherBlob(IDictionary<DateTime, BomDayData> dailyData)
        {
            foreach (var dayData in dailyData)
            {
                DateTime date = dayData.Key;
                BomDayData value = dayData.Value;

                if (!yearlyWeatherData.ContainsKey(date.Year))
                {
                    WeatherDataForYear yearlyData = new WeatherDataForYear(date.Year);
                    yearlyWeatherData.Add(date.Year, yearlyData);
                }

                yearlyWeatherData[date.Year].AddDayData(date, value);
            }
            
            foreach (var yearData in yearlyWeatherData)
            {
                WeatherDataForYear year = yearData.Value;
                year.generateStatistics();
            }
        }

        public string getJson()
        {
            JsonSerializerSettings settings = new JsonSerializerSettings { Converters = new[] { new BomJsonConverter() }, Formatting = Formatting.Indented };
            var jsonString = JsonConvert.SerializeObject(this, settings);
            return jsonString;
        }
    }

    public struct BomDayData
    {
        // We likely care about other information in future, e.g. recording quality, so I chose a struct
        public decimal rainfall;

        public BomDayData(decimal rainfall_c)
        {
            rainfall = rainfall_c;
        }
    }
}
