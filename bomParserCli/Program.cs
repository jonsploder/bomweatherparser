﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using bomWeatherParser;

namespace bomParserCli
{
    class Program
    {
        static void Main(string[] args)
        {
            string csvPath;
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: bomParserCli.exe relative_path");
                return;
            }
            else
            {
                csvPath = args[0];
            }

            char separator = ',';
            Console.WriteLine(Directory.GetCurrentDirectory());

            string json = BomWeatherParser.bomCsvToJson(csvPath, separator);
            Console.WriteLine(json);
        }
    }
}
