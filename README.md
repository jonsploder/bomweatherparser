# bomWeatherParser

# Assumptions

- The average daily rainfall should be represented with 9 decimal places always, regardless of trailing 0's for consistency.
- We don't particularly care about cultures, for example the '.' being replaced with ',' for decimal places as is convention in the US (I think?). 
Perhaps this code compiled with different machine culture info may break the unit tests because of it -> some tests are simply string comparison.
- Discount days entirely that have no data for them. For example 2018/6/24 is empty, so we can't be sure it's 0 rainfall or not. So we can't count it in the AverageRainfall etc.
- We don't care for malformed input; if someone wants to put in a different format to csv or the output format from BOM changes, then it will error.
- We assume the csv starts with the csv headers

# How to run

After compiling (say in debug mode), run the following command from your relative path to this directory:
cd to ../bomParserCli/bin/debug/
./bomParserCli <relative path to input file>

For example (with the directory structure I've pushed to bitbucket):
C:\Users\John\Documents\Visual Studio 2015\Projects\bomWeatherParser\bomParserCli\bin\Debug>bomWeatherParser.exe ../../../exampleProvidedData.csv