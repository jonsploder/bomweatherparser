﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bomWeatherParser
{
    public class WeatherDataForPeriod
    {
        public DateTime? firstRecordedDate = null;
        public DateTime? lastRecordedDate = null;
        public decimal totalRainfall;
        public decimal averageDailyRainfall;
        public int daysWithNoRainfall;
        public int daysWithRainfall;
        protected const int monthsInYear = 12;
        protected const int maxDaysInMonth = 31;


        protected void CalculateAverageRainfall()
        {
            int daysRecorded = (daysWithRainfall + daysWithNoRainfall);
            averageDailyRainfall = (daysRecorded == 0) ? 0 : (totalRainfall / daysRecorded);
        }

        protected void UpdateRecordedBoundaryDates(DateTime date)
        {
            if (firstRecordedDate == null || date < firstRecordedDate)
            {
                firstRecordedDate = date;
            }
            if (lastRecordedDate == null || date > lastRecordedDate)
            {
                lastRecordedDate = date;
            }

        }
    }

    public class WeatherDataForYear : WeatherDataForPeriod
    {
        public int year;
        public WeatherDataForMonth[] months;

        public WeatherDataForYear(int year_c)
        {
            year = year_c;
            months = new WeatherDataForMonth[12];
            for (int i = 0; i < monthsInYear; i += 1)
            {
                months[i] = null;
            }
        }

        public void AddDayData(DateTime date, BomDayData data)
        {
            UpdateRecordedBoundaryDates(date);
            if (months[date.Month - 1] == null)
            {
                months[date.Month - 1] = new WeatherDataForMonth(date.Year, date.Month);
            }
            months[date.Month - 1].AddDayData(date, data);
        }

        public void generateStatistics()
        {
            for (int i = 0; i < monthsInYear; i += 1)
            {
                if (months[i] != null)
                {
                    months[i].generateStatistics();
                    totalRainfall += months[i].totalRainfall;
                    daysWithRainfall += months[i].daysWithRainfall;
                    daysWithNoRainfall += months[i].daysWithNoRainfall;
                }
            }
            CalculateAverageRainfall();
        }
    }

    public class WeatherDataForMonth : WeatherDataForPeriod
    {
        public int month;
        private int year;
        private BomDayData[] dayData;

        public WeatherDataForMonth(int year_c, int month_c)
        {
            year = year_c;
            month = month_c;
            dayData = new BomDayData[31];
            for (int i = 0; i < maxDaysInMonth; i += 1)
            {
                dayData[i].rainfall = -1;
            }
        }

        public void AddDayData(DateTime date, BomDayData data)
        {
            UpdateRecordedBoundaryDates(date);
            dayData[date.Day - 1] = data;
        }

        public void generateStatistics()
        {
            for (int i = 0; i < maxDaysInMonth; i += 1)
            {
                decimal rainfall = ((BomDayData)dayData[i]).rainfall;
                if (rainfall == 0)
                {
                    daysWithNoRainfall += 1;
                }
                else if (rainfall != -1)
                {
                    daysWithRainfall += 1;
                    totalRainfall += rainfall;
                }
            }
            CalculateAverageRainfall();
        }
    }

}
